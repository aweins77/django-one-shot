from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "biglists": lists,
    }
    return render(request, "todos/lists.html", context)


def todo_list_detail(request, id):
    todo_lists = get_object_or_404(TodoList,id=id)
    context = {"detailed_view" : todo_lists}
    return render(request, "todos/detail.html", context)
